import React, { Component } from 'react'
import { StyleSheet, View } from 'react-native'
import { Content, Text, Item, Icon, Input, Button } from 'native-base'

export default class ChangePasswordLayout extends Component{
    static navigationOptions = ({ navigation }) => ({
      title: 'Change Password',
      headerRight: 
        <Button 
          transparent 
          onPress={() => navigation.dispatch({type: 'Navigation'})}>
          <Text>Save</Text>
        </Button>
    });
  
    render() {
      return (
        <Content>
          <View style={styles.editContainer}>
            <Item>
              <Icon active name='ios-lock-outline' />
              <Input placeholder='Current Password' secureTextEntry={true} />
            </Item>
            <Item>
              <Icon active name='ios-unlock-outline' />
              <Input placeholder='New Password' secureTextEntry={true} />
            </Item>
            <Item>
              <Icon active name='ios-unlock-outline' />
              <Input placeholder='Confirm New Password' secureTextEntry={true} />
            </Item>
          </View>
        </Content>
      )
    }
  }
  
  const styles = StyleSheet.create({
    editContainer: {
      backgroundColor: '#FFF',
      paddingTop: 10,
      paddingBottom: 25,
      paddingHorizontal: 20,
    },
  })