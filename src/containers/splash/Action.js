import * as ActionTypes from '../../constants/actionTypes'
import { AsyncStorage } from 'react-native'
import { getUserAuth, getUser } from '../login/Action'

export const checkUserLogin = (navigation) => (dispatch, _) => (async () => {
  await new Promise(resolve => setTimeout(resolve, 2000))
  try {
    const data = await AsyncStorage.multiGet(['token', 'id'])
    const token = data[0][1] ? data[0][1] : null
    if(token != null) {
      const userAuth = { token: data[0][1], id: data[1][1]}
      await dispatch(getUserAuth(userAuth))
      await dispatch(getUser(data[1][1]))
      await dispatch(navigationState())
      await navigation.dispatch({type: 'Navigation'})
    } else {
      throw null
    }
  }
  catch (e) {
    console.log(e)
    await dispatch(loginState())
    await navigation.dispatch({type: 'Login'})
  }
})()

export const loginState = () => {
  return { type: ActionTypes.LOGIN_STATE }
}

export const navigationState = () => {
  return { type: ActionTypes.NAVIGATION_STATE }
}