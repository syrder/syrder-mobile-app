import React, { Component } from 'react'
import { StyleSheet, Text, Image, ActivityIndicator } from 'react-native'
import { Container } from 'native-base'

export default class SplashLayout extends Component {
  constructor(props) {
    super(props)
  }

  componentWillMount() {
    const { checkUserLogin, navigation } = this.props
    checkUserLogin(navigation)
  }

  static navigationOptions = ({ navigation }) => ({
    header: null,
  })
  
  render() {
    const { userState } = this.props
    return (
      <Image source={require('./splashAsset.jpeg')} style={styles.container}>
        <Text style={styles.textTitle}>Syrder</Text>
        <ActivityIndicator color='white'/>
      </Image>
    )
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    width: null,
    height: null,
    resizeMode: 'cover',
    alignItems: 'center',
    justifyContent: 'center'
  },
  textTitle: {
    fontSize: 36,
    backgroundColor: 'transparent',
    color: 'white',
    fontWeight: 'bold',
    marginBottom: 5,
  }
})