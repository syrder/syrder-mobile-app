import { createStructuredSelector } from 'reselect'

const userState = state => state.user.userState

export default createStructuredSelector({
  userState
})