import React, { Component } from 'react'
import { StyleSheet } from 'react-native'
import { Container, Button, Text, Icon, Footer, FooterTab } from 'native-base'
import MenuContainer from '../menu/Container'
import HomeContainer from '../home/Container'
import SettingsContainer from '../settings/Container'
import OrdersContainer from '../orders/Container'

export default class NavigationLayout extends Component {
  constructor(props) {
    super(props)
    this.state = {
      index: 0
    }
  }

  static navigationOptions = ({ navigation }) => ({
    visible: false,
  })

  render() {
    const { index } = this.state
    let AppComponent = null
    switch(index){
      case 0:
        AppComponent = HomeContainer
        break
      case 1:
        AppComponent = MenuContainer
        break
      case 2:
        AppComponent = OrdersContainer
        break
      case 3:
        AppComponent = SettingsContainer
        break
    }
    return (
      <Container>
        <AppComponent/>
        <Footer>
          <FooterTab>
            <Button onPress={() => this.setState({index:0})} vertical active={index === 0}>
              <Icon name="home" />
              <Text>Home</Text>
            </Button>
            <Button onPress={() => this.setState({index:1})} vertical active={index === 1}>
              <Icon name="restaurant" />
              <Text>Menu</Text>
            </Button>
            <Button onPress={() => this.setState({index:2})} vertical active={index === 2}>
              <Icon active name="ios-clipboard-outline" />
              <Text>Orders</Text>
            </Button>
            <Button onPress={() => this.setState({index:3})} vertical active={index === 3}>
              <Icon name="settings" />
              <Text>Settings</Text>
            </Button>
          </FooterTab>
        </Footer>
      </Container>
    )
  }
}