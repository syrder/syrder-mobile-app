
import * as ActionTypes from '../../constants/actionTypes'
import { Alert, AsyncStorage } from 'react-native'
import * as Config from '../../constants/config'


export const loginUser = (username, password, navigation) => (dispatch, _) => (async () => {
  try {
    dispatch(loginRequested())
    const response = await fetch(`${Config.API_ENDPOINT}/auth/login`, {
      method: Config.POST,
      headers: Config.HEADER,

      body: JSON.stringify({
        username: username,
        password: password,
      })
    })
    const responseJson = await response.json()
    console.log(responseJson)
    const userAuth = { token: responseJson.token, id: responseJson.id }
    if(responseJson) {
      AsyncStorage.multiSet([
        ['token', responseJson.token],
        ['id', responseJson.id ],
      ])
    }
  
    await dispatch(getUserAuth(userAuth))
    await dispatch(getUser(responseJson.id))
    dispatch(loginSuccess())
    navigation.dispatch({type: 'Navigation'})
  }
  catch (e) {
    Alert.alert('Error', 'Authentication failed')
    console.log(e)
    dispatch(loginFailed())
  }
})()

export const getUser = (id) => (dispatch, _) => (async () => {
<<<<<<< HEAD
  const response = await fetch(`http://192.168.178.34:4000/api/users/${id}`, {
    method: 'GET',
=======
  const response = await fetch(`${Config.API_ENDPOINT}/users/${id}`, {
    method: Config.GET,
>>>>>>> feature/FEC001-simple-authentication-flow
  })
  const responseJson = await response.json()
  await dispatch(getUserProfile(responseJson))
})()

export const validateUsername = (username) => (dispatch, _) => (() => {
  username === '' ? dispatch(invalidUsername()) : dispatch(validUsername())
})()

export const validatePassword = (password) => (dispatch, _) => (() => {
  password === '' ? dispatch(invalidPassword()) : dispatch(validPassword())
})()

export const loginRequested = () => {
  return { type: ActionTypes.LOGIN_REQUESTED }
}

export const loginSuccess = () => {
  return { type: ActionTypes.LOGIN_SUCCESS }
}

export const loginFailed = () => {
  return { type: ActionTypes.LOGIN_FAILED}
}

export const validUsername = () => {
  return { type: ActionTypes.VALID_LOGIN_USERNAME }
}

export const invalidUsername = () => {
  return { type: ActionTypes.INVALID_LOGIN_USERNAME }
}

export const validPassword = () => {
  return { type: ActionTypes.VALID_LOGIN_PASSWORD }
}

export const invalidPassword = () => {
  return { type: ActionTypes.INVALID_LOGIN_PASSWORD }
}

export const getUserAuth = (payload) => {
  return {
    type: ActionTypes.GET_USER_AUTH,
    payload
  }
}

export const getUserProfile = (payload) => {
  return {
    type: ActionTypes.GET_USER_PROFILE,
    payload
  }
}
