import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import * as Action from './Action'
import LoginLayout from './Layout'
import selector from './selector'

export default connect(
	selector, 
	dispatch => bindActionCreators(Action, dispatch)
)(LoginLayout)