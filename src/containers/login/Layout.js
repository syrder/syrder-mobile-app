import React, { Component } from 'react'
import { StyleSheet, ActivityIndicator, View, Text } from 'react-native'
import { Container, Form, Item, Input, Label, Button,} from 'native-base'

export default class LoginLayout extends Component {
  constructor(props) {
    super(props)
    this.state = {
      username: '',
      usernameFocus: false,
      password: '',
      passwordFocus: false,
    }
  }

  static navigationOptions = ({ navigation }) => {
    return { title: 'Login'}
  }

  render = () => {
    const { login, loginUser, validateUsername, validatePassword, navigation } = this.props
    const { usernameFocus , passwordFocus, username, password } = this.state
    return (
      <Container>
        <View style={styles.container}>
          <Form>
            <Item floatingLabel error={!login.validUsername && usernameFocus}>
              <Label>Username</Label>
              <Input 
                onEndEditing={() => this.setState({ usernameFocus: true})} 
                onChangeText={(username) => this.setState({username}, () => validateUsername(username))}
                clearButtonMode='while-editing'
              />   
            </Item>
            {!login.validUsername && usernameFocus && 
              <Text style={styles.errorLabel}>Please enter the username</Text>
            }
            <Item floatingLabel error={!login.validPassword && passwordFocus}>
              <Label>Password</Label>
              <Input 
                secureTextEntry={true}
                onEndEditing={() => this.setState({ passwordFocus: true})}
                onChangeText={(password) => this.setState({password}, () => validatePassword(password))}
                clearButtonMode='while-editing'
              />
              
            </Item>
            {!login.validPassword && passwordFocus && 
              <Text style={styles.errorLabel}>Please enter the password</Text>
            }
          </Form>
          <Button 
            disabled={!login.validPassword || !login.validUsername} 
            block 
            primary={true} 
            style={(!login.validPassword || !login.validUsername) ? styles.disabledLoginBtn :  styles.loginBtn} 
            onPress={() => loginUser(username, password, navigation)}
          >
            { login.isProcessing ? 
              <ActivityIndicator color='white'/> : 
              <Text style={styles.loginLabel}>Sign Up</Text> 
            }
          </Button>
        </View>
      </Container>

    )
  }
}

const styles = StyleSheet.create({
  loginBtn: {
    marginTop: 50,
  },
  disabledLoginBtn: {
    marginTop: 50,
    backgroundColor: '#B8B8B8',
  },
  loginLabel: {
    color: '#FFF',
  },
  errorLabel: {
    color: '#D52222',
    marginHorizontal: 20,
  },
  container: {
    flex:1,
    marginHorizontal: 15,
  },
})