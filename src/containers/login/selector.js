import { createStructuredSelector } from 'reselect'

const login = state => state.authentication.login
const userState = state => state.user.userState

export default createStructuredSelector({
  login,
})