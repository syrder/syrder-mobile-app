import React, { Component } from 'react'
import { StyleSheet, ActivityIndicator, View, Text } from 'react-native'
import { Container, Form, Item, Input, Label, Button,} from 'native-base'

export default class SignupLayout extends Component {
  constructor(props) {
    super(props)
    this.state = {
      username: '',
      usernameFocus: false,
      password: '',
      passwordFocus: false,
    }
  }

  static navigationOptions = ({ navigation }) => ({
    title: 'Sign Up',
  })

  render() {
    const { register, registerUser, validateUsername, validatePassword, navigation } = this.props
    const { usernameFocus , passwordFocus, username, password } = this.state
    return (
      <Container>
        <View style={styles.container}>
          <Form>
            <Item floatingLabel error={!register.validUsername && usernameFocus}>
              <Label>Username</Label>
              <Input 
                onEndEditing={() => this.setState({ usernameFocus: true})} 
                onChangeText={(username) => this.setState({username}, () => validateUsername(username))}
                clearButtonMode='while-editing'
              />   
            </Item>
            {!register.validUsername && usernameFocus && 
              <Text style={styles.errorLabel}>Please enter the username</Text>
            }
            <Item floatingLabel error={!register.validPassword && passwordFocus}>
              <Label>Password</Label>
              <Input 
                secureTextEntry={true}
                onEndEditing={() => this.setState({ passwordFocus: true})}
                onChangeText={(password) => this.setState({password}, () => validatePassword(password))}
                clearButtonMode='while-editing'
              />
              
            </Item>
            {!register.validPassword && passwordFocus && 
              <Text style={styles.errorLabel}>Please enter the password</Text>
            }
          </Form>
          <Button 
            disabled={!register.validPassword || !register.validUsername} 
            block 
            primary={true} 
            style={(!register.validPassword || !register.validUsername) ? styles.disabledSignupBtn :  styles.signupBtn} 
            onPress={() => registerUser(username, password, navigation)}>
            { register.isProcessing ? 
              <ActivityIndicator color='white'/> : 
              <Text style={styles.signupLabel}>Sign Up</Text> }
          </Button>
        </View>
      </Container>
    )
  }
}

const styles = StyleSheet.create({
  signupBtn: {
    marginTop: 50,
  },
  disabledSignupBtn: {
    marginTop: 50,
    backgroundColor: '#B8B8B8',
  },
  signupLabel: {
    color: '#FFF',
  },
  errorLabel: {
    color: '#D52222',
    marginHorizontal: 20,
  },
  container: {
    flex:1,
    marginHorizontal: 15,
  },
})