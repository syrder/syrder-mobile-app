import { createStructuredSelector } from 'reselect'

const register = state => state.authentication.register

export default createStructuredSelector({
  register
})