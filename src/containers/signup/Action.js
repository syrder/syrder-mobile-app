import * as ActionTypes from '../../constants/actionTypes'
import * as Config from '../../constants/config'

export const registerUser = (username, password, navigation) => (dispatch, _) => (async () => {
  try {
    dispatch(registerRequested())
    const response = await fetch(`${Config.API_ENDPOINT}/users`, {
      method: Config.POST,
      headers: Config.HEADER,
      body: JSON.stringify({
        username: username,
        password: password,
      })
    })
    const responseJson = await response.json()
    await dispatch(registerSuccess())
    navigation.dispatch(loginNavigation())    
  }
  catch (e) {
    Alert.alert('Error', 'Register failed')
    dispatch(registerFailed())
  }
})()

export const validateUsername = (username) => (dispatch, _) => (() => {
  username === '' ? dispatch(invalidUsername()) : dispatch(validUsername())
})()

export const validatePassword = (password) => (dispatch, _) => (() => {
  password === '' ? dispatch(invalidPassword()) : dispatch(validPassword())
})()

export const registerRequested = () => {
  return { type: ActionTypes.REGISTER_REQUESTED }
}

export const registerSuccess = () => {
  return { type: ActionTypes.REGISTER_SUCCESS }
}

export const registerFailed = () => {
  return { type: ActionTypes.REGISTER_FAILED}
}

export const validUsername = () => {
  return { type: ActionTypes.VALID_USERNAME }
}

export const invalidUsername = () => {
  return { type: ActionTypes.INVALID_USERNAME }
}

export const validPassword = () => {
  return { type: ActionTypes.VALID_PASSWORD }
}

export const invalidPassword = () => {
  return { type: ActionTypes.INVALID_PASSWORD }
}

export const loginNavigation = () => {
  return { type: ActionTypes.LOGIN_NAVIGATION }
}
