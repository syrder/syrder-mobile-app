
import * as ActionTypes from '../../constants/actionTypes'
import { AsyncStorage } from 'react-native'

export const logout = () => (dispatch, _) => (async () => {
  await AsyncStorage.multiRemove(['token', 'id'])
  await dispatch(logoutAction())
  await dispatch(logoutNavigation())
})()

export const changePassword = () => (dispatch, _) => (async () => {
  await dispatch(changePasswordNavigation())
})()

export const profile = () => (dispatch, _) => (async () => {
  await dispatch(profileNavigation())
})()

export const logoutAction = () => {
  return { type: ActionTypes.LOGOUT }
}

export const logoutNavigation = () => {
  return { type: 'Logout' }
}

export const changePasswordNavigation = () => {
  return { type: 'ChangePassword'}
}

export const profileNavigation = () => {
  return { type: 'Profile'}
}