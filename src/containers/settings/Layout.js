import React, { Component } from 'react'
import { StyleSheet, TouchableOpacity } from 'react-native'
import { Container, Content, List, Separator, Text } from 'native-base'
import ListWithLeftRightIcon from '../../components/ListWithLeftRightIcon'


export default class SettingsLayout extends Component {
  constructor(props) {
    super(props)
  }

  render() {
    const { logout, dispatch, changePassword, profile } = this.props
    return (
      <Container>
        <Content>
          <List style={{backgroundColor:'white'}}>
            <Separator bordered>
              <Text>APP SETTINGS</Text>
            </Separator>
            <ListWithLeftRightIcon
              leftIcon="home"
              rightIcon="arrow-forward"
              listText="Home Settings"
            />
            <ListWithLeftRightIcon
              leftIcon="restaurant"
              rightIcon="arrow-forward"
              listText="Menu Settings"
            />
            <ListWithLeftRightIcon
              leftIcon="ios-clipboard-outline"
              rightIcon="arrow-forward"
              listText="Order Settings"
            /> 
            <Separator bordered>
              <Text>CUSTOMER SETTINGS</Text>
            </Separator>
            <ListWithLeftRightIcon
              leftIcon="contact"
              rightIcon="arrow-forward"
              listText="Change Profile"
              onPress={() => dispatch(profileNavigation())}
            />
            <ListWithLeftRightIcon
              leftIcon="unlock"
              rightIcon="arrow-forward"
              listText="Change Password"
              onPress={() => dispatch(changePasswordNavigation())}
            />
            <ListWithLeftRightIcon
              leftIcon="help-circle"
              rightIcon="arrow-forward"
              listText="Help and Assistance"
            />
            <ListWithLeftRightIcon
              leftIcon="log-out"
              rightIcon="arrow-forward"
              listText="Logout"
              onPress={() => logout()}
            />
          </List>
        </Content>
      </Container>
    )
  }
}