import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import * as Action from './Action'
import SettingsLayout from './Layout'

export default connect(
	null, 
	dispatch => bindActionCreators(Action, dispatch)
)(SettingsLayout)