import React, { Component } from 'react'
import { StyleSheet, FlatList, View } from 'react-native'
import { Container, Header, Body, Content, Text, Card, CardItem, List, ListItem, Tab, Tabs, Left, Right } from 'native-base'
import OrderData from './data.js'

export default class OrdersLayout extends Component {
  constructor(props) {
    super(props)
  }

  currentOrder = (data) => {
    return (
      <ListItem>
        <Body>
          <Text>Table {data.item.table}</Text>
          <Text note>Order #{data.item.key}</Text>
        </Body>
        <Right>
          <Text>{data.item.openTime} pm</Text>
          <Text note>{data.item.openDate}</Text>
        </Right>
      </ListItem>
    )
  }

  pastOrder = (data) => {
    return (
      <ListItem>
        <Body>
          <Text>Order #{data.item.key}</Text>
        </Body>
        <Right>
          <Text>{data.item.openDate}</Text>
          <Text note>{data.item.openTime} pm</Text>
        </Right>
      </ListItem>
    )
  }

  render() {
    return (
      <Container>
        <Tabs initialPage={0}>
          <Tab heading="Current Orders">
            <List>
              <FlatList
                data={OrderData.orderData}
                renderItem={this.currentOrder}
              />
            </List>
          </Tab>
          <Tab heading="Past Orders">
            <List>
              <FlatList
                data={OrderData.orderData}
                renderItem={this.pastOrder}
              />
            </List>
          </Tab>
        </Tabs>
      </Container>
    )
  }
}

const styles = StyleSheet.create({
  loginBtn: {
    marginHorizontal: 15,
    marginTop: 50,
  },
})