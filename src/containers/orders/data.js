export default {
  orderData: [
    {
      key: '10000', 
      table: '22',
      openTime: '1:33',
      openDate: '12 August'
    },
    {
      key: '10001', 
      table: '9',
      openTime: '1:45',
      openDate: '12 August'
    },
    {
      key: '10002', 
      table: '15',
      openTime: '1:55',
      openDate: '12 August'
    },
  ]
}