import React, { Component } from 'react'
import { Container, Body, Content, Text, Card, CardItem, List, ListItem } from 'native-base'


export default class HomeLayout extends Component {
  constructor(props) {
    super(props)
  }

  render() {
    return (
      <Container>
        <Content>
          <Card>
            <CardItem header>
              <Text>Currently Serving:</Text>
            </CardItem>
            <CardItem>
              <Body>
                <Text>
                  Table number 22, 24, 25
                </Text>
              </Body>
            </CardItem>
          </Card>
          <Card>
            <CardItem header>
              <Text>Table 22</Text>
            </CardItem>
            <CardItem>
              <Body>
                <List>
                  <ListItem>
                    <Text>Special Fried Rice x 1 - $12.00</Text>
                  </ListItem>
                  <ListItem>
                    <Text>Grilled Chicken with Sauce x 1 - $21.00</Text>
                  </ListItem>
                  <ListItem>
                    <Text>Special Creamy Pasta x 1 - $16.00</Text>
                  </ListItem>
                </List>
              </Body>
            </CardItem>
            <CardItem footer>
              <Text>Total: $49.00</Text>
            </CardItem>
          </Card>
        </Content>
      </Container>
    )
  }
}
