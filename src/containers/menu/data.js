export default {
  menuData: [
    {
        key: 'CHICKEN', 
        data: [{
            key: '1',
            title: 'Stir Fry Chicken',
        },
        {
            key: '2',
            title: 'Crispy Fried Chicken',
        },
        ]
    },
    {
        key: 'BEEF',
        data: [
        {
            key: '1',
            title: 'Stir Fry Beef',
        },
        {
            key: '2',
            title: 'Beef Bulgogi',
        },
        ]
    }
    ]
}