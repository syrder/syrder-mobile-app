import React, { Component } from 'react'
import { StyleSheet, SectionList, View, TextInput, TouchableOpacity } from 'react-native'
import { Container, Content, Header, Icon, Input, Item, ListItem, Text, Separator } from 'native-base'
import MenuData from './data.js'

export default class MenuLayout extends Component {
  constructor(props) {
    super(props)
    this.state = {
      searchInput: ''
    }
  }

  handleSearchInput = (text) => {
    this.setState({
      searchInput: text
    })
  }
  
  menuItem = (data) => {
    return (
        <ListItem button onPress={this.handleMenuItem}>
          <Text>{data.item.title}</Text>
        </ListItem>
    )
  }

  menuSection = (data) => {
    if(data.section.data.length > 0) {
      return (
        <Separator bordered>
          <Text>{data.section.key}</Text>
        </Separator>
      )
    }
  }

  filterMenu = (MenuData, text) => {
    return MenuData.menuData.map((menu) => {
      return {
        key: menu.key,
        data: menu.data.filter((item) => {
          return item.title.toLowerCase().indexOf(text.toLowerCase()) !== -1
        })
      }
    })
  }

  displayMenu = () => {
    if(MenuData.menuData.length > 0) {
      return (
        <SectionList
          sections={this.filterMenu(MenuData, this.state.searchInput)}
          renderItem={this.menuItem}
          renderSectionHeader={this.menuSection}
        />
      )
    } else {
      return (
        <Text>No menu items available</Text>
      )
    }
  }

  static navigationOptions = ({ navigation }) => ({
    title: 'Menu',
  })

  render() {
    return (
      <Container>
        <Content>
          <View style={styles.searchSection}>
            <View style={styles.searchBarContainer}>
              <Icon name='ios-search' style={styles.searchIcon} />
              <TextInput placeholder='Search' placeholderTextColor="#777" style={styles.searchBar} onChangeText={this.handleSearchInput} />
            </View>
          </View>
          <View style={styles.menuSection}>
            {this.displayMenu()}
          </View>
        </Content>
      </Container>
    )
  }
}

const styles = StyleSheet.create({
  searchSection: {
    backgroundColor: 'white',
    padding: 8,
    borderWidth: 1,
    borderColor: '#EEE',
  },

  searchBarContainer: {
    flexDirection: 'row',
    backgroundColor: '#EEE',
    padding: 5,
    borderRadius: 16,
  },

  searchBar: {
    flex: 1,
  },

  searchIcon: {
    fontSize: 20,
    marginHorizontal: 8,
    color: '#777',
  },

  menuSection: {
    backgroundColor: '#FFF',
  }
})