import React, { Component } from 'react'
import { StyleSheet, View } from 'react-native'
import { Content, Text, Item, Icon, Input, Button } from 'native-base'

export default class ProfileLayout extends Component{
  constructor(props) {
    super(props)
  }

  static navigationOptions = ({ navigation }) => ({
    title: 'Profile',
    headerRight: 
      <Button 
        transparent 
        onPress={() => navigation.dispatch({type: 'Navigation'})}>
        <Text>Save</Text>
      </Button>
  })

  render() {
    return (
      <Content>
        <View style={styles.editContainer}>
          <Item>
            <Icon active name='ios-person-outline' />
            <Input placeholder='Username' editable={false} />
          </Item>
          <Item>
            <Icon active name='ios-home-outline' />
            <Input placeholder='Restaurant Name' />
          </Item>
          <Item>
            <Icon active name='ios-navigate-outline' />
            <Input placeholder='Address' />
          </Item>
          <Item>
            <Icon active name='ios-call-outline' />
            <Input placeholder='Phone Number' keyboardType='phone-pad' />
          </Item>
        </View>
      </Content>
    )
  }
}

const styles = StyleSheet.create({
  editContainer: {
    backgroundColor: '#FFF',
    paddingTop: 10,
    paddingBottom: 25,
    paddingHorizontal: 20,
  },
})