import { combineReducers } from 'redux'
import * as ActionTypes from '../constants/actionTypes'

const userAuth = (state = {}, action = {}) => {
  switch(action.type) {
    case ActionTypes.GET_USER_AUTH:
      return action.payload
    case ActionTypes.LOGOUT:
      return {}
    default:
      return state
  }
}

const userProfile = (state = {}, action = {}) => {
  switch(action.type) {
    case ActionTypes.GET_USER_PROFILE:
      return action.payload
    case ActionTypes.LOGOUT:
      return {}
    default:
      return state
  }
}

const userState = (state = false, action = {}) => {
  switch(action.type) {
    case ActionTypes.LOGIN_STATE:
    case ActionTypes.NAVIGATION_STATE:
      return true
    default:
      return state
  }
}


export default combineReducers({
  userAuth,
  userProfile,
  userState,
})