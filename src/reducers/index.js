import { combineReducers } from 'redux'
import * as ActionTypes from '../constants/actionTypes'
import { NavigationActions } from 'react-navigation'
import authentication from './authentication'
import user from './user'
import { AppNavigator } from '../routes'

// Start with two routes: The Main screen, with the Login screen on top.
const firstAction = AppNavigator.router.getActionForPathAndParams('Splash')
const initialNavState = AppNavigator.router.getStateForAction(
  firstAction
)

const nav = (state = initialNavState, action) => {
  let nextState;
  switch (action.type) {
    case 'Logout':
      nextState = AppNavigator.router.getStateForAction(
        NavigationActions.reset({
          index: 0,
          actions: [
            NavigationActions.navigate({ routeName: 'Login'})
          ]
        }),
        state
      )
      break;
    case 'Signup':
      nextState = AppNavigator.router.getStateForAction(
        NavigationActions.navigate({
          routeName: 'Signup'
        }),
        state
      );
      break;
    case 'Profile':
      nextState = AppNavigator.router.getStateForAction(
        NavigationActions.navigate({
          routeName: 'Profile'
        }),
        state
      );
      break;
    case 'ChangePassword':
      nextState = AppNavigator.router.getStateForAction(
        NavigationActions.navigate({
          routeName: 'ChangePassword'
        }),
        state
      );
      break;
    case 'Login':
      nextState = AppNavigator.router.getStateForAction(
        NavigationActions.reset({
          index: 0,
          actions: [
            NavigationActions.navigate({ routeName: 'Login'})
          ]
        }),
        state
      )
      break;
    case 'Navigation':
      nextState = AppNavigator.router.getStateForAction(
        NavigationActions.reset({
          index: 0,
          actions: [
            NavigationActions.navigate({ routeName: 'Navigation'})
          ]
        }),
        state
      );
      break;
    default:
      nextState = AppNavigator.router.getStateForAction(action, state);
      break;
  }

  // Simply return the original `state` if `nextState` is null or undefined.
  return nextState || state;
}

const rootReducer = combineReducers({
  authentication,
  nav,
  user,
})

export default rootReducer
