import { combineReducers } from 'redux'
import * as ActionTypes from '../constants/actionTypes'

const login = (state = {isProcessing: false, validUsername: false, validPassword: false }, action = {}) => {
  switch(action.type) {
  case ActionTypes.LOGIN_REQUESTED:
    return {...state, isProcessing: true }
  case ActionTypes.LOGIN_SUCCESS:
  case ActionTypes.LOGIN_FAILED:
    return {...state, isProcessing: false }
  case ActionTypes.VALID_LOGIN_USERNAME:
    return {...state, validUsername: true }
  case ActionTypes.INVALID_LOGIN_USERNAME:
    return {...state, validUsername: false }
  case ActionTypes.VALID_LOGIN_PASSWORD:
    return {...state, validPassword: true }
  case ActionTypes.INVALID_LOGIN_PASSWORD:
    return {...state, validPassword: false }
  default:
    return state
  }
}

const register = (state = {isProcessing: false, validUsername: false, validPassword: false }, action = {}) => {
  switch(action.type) {
  case ActionTypes.REGISTER_REQUESTED:
    return {...state, isProcessing: true }
  case ActionTypes.REGISTER_SUCCESS:
  case ActionTypes.REGISTER_FAILED:
    return {...state, isProcessing: false }
  case ActionTypes.VALID_USERNAME:
    return {...state, validUsername: true }
  case ActionTypes.INVALID_USERNAME:
    return {...state, validUsername: false }
  case ActionTypes.VALID_PASSWORD:
    return {...state, validPassword: true }
  case ActionTypes.INVALID_PASSWORD:
    return {...state, validPassword: false }
  default:
    return state
  }
}

export default combineReducers({
  login,
  register
})