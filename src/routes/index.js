import React from 'react'
import { connect } from 'react-redux'
import { addNavigationHelpers, StackNavigator } from 'react-navigation'

import LoginContainer from '../containers/login/Container'
import SignupContainer from '../containers/signup/Container'
import MenuContainer from '../containers/menu/Container'
import NavigationContainer from '../containers/navigation/Container'
import SplashContainer from '../containers/splash/Container'
import ProfileContainer from '../containers/profile/Container'
import ChangePasswordContainer from '../containers/changePassword/Container'

export const AppNavigator = StackNavigator({
  Login: { screen: LoginContainer },
  Signup: { screen: SignupContainer },
  Menu: { screen: MenuContainer },
  Navigation: { screen: NavigationContainer },
  Splash: { screen: SplashContainer },
  Profile: { screen: ProfileContainer },
  ChangePassword: { screen: ChangePasswordContainer },
})

const AppWithNavigationState = ({ dispatch, nav }) => (
  <AppNavigator navigation={addNavigationHelpers({ dispatch, state: nav })} />
)

const mapStateToProps = state => ({
  nav: state.nav,
});

export default connect(mapStateToProps)(AppWithNavigationState);
