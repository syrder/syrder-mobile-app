import React, { Component } from 'react'
import { Body, ListItem, Text, Icon, Right, Left } from 'native-base'

export default class ListWithLeftRightIcon extends Component {
  constructor(props) {
    super(props)
  }

  render() {
    const { leftIcon, listText, rightIcon } = this.props
    return (
      <ListItem icon>
        <Left>
          <Icon name={leftIcon}/>
        </Left>
        <Body>
          <Text>{listText}</Text>
        </Body>
        <Right>
          <Icon name={rightIcon} />
        </Right>
      </ListItem>
    )
  }
}
