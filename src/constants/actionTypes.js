// Login
export const LOGIN_REQUESTED = 'syrder/LOGIN_REQUESTED'
export const LOGIN_SUCCESS = 'syrder/LOGIN_SUCCESS'
export const LOGIN_FAILED = 'syrder/LOGIN_FAILED'
export const VALID_LOGIN_USERNAME = 'syrder/VALID_LOGIN_USERNAME'
export const VALID_LOGIN_PASSWORD = 'syrder/VALID_LOGIN_PASSWORD'
export const INVALID_LOGIN_USERNAME = 'syrder/INVALID_LOGIN_USERNAME'
export const INVALID_LOGIN_PASSWORD = 'syrder/INVALID_LOGIN_PASSWORD'

// Register
export const REGISTER_REQUESTED = 'syrder/REGISTER_REQUESTED'
export const REGISTER_SUCCESS = 'syrder/REGISTER_SUCCESS'
export const REGISTER_FAILED = 'syrder/REGISTER_FAILED'
export const VALID_USERNAME = 'syrder/VALID_USERNAME'
export const VALID_PASSWORD = 'syrder/VALID_PASSWORD'
export const INVALID_USERNAME = 'syrder/INVALID_USERNAME'
export const INVALID_PASSWORD = 'syrder/INVALID_PASSWORD'

// User
export const GET_USER_PROFILE = 'syrder/GET_USER_PROFILE'
export const GET_USER_AUTH = 'syrder/GET_USER_AUTH'
export const LOGOUT = 'syrder/LOGOUT'
export const LOGIN_STATE = 'syrder/LOGIN_STATE'
export const NAVIGATION_STATE = 'syrder/NAVIGATION_STATE'