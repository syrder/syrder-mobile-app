This project is Syrder-mobile-app.

Project version: 0.1

Key features: 

* Containers for login, signup, orders, menu, profile, settings, splash, navigation
* Reducers for navigation, authentication, user
* Constants for actionTypes, colors, config, routes
* Components for list
* Store using configureStore
* Routes using React-navigation
* Using base of create-react-native-app
* Expo for deploying application

Developed by Timothy Alfares & Jessica Wiradinata

Copyright 2017.