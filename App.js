import React from 'react'
import { AppRegistry } from 'react-native'
import { Provider } from 'react-redux'

import store from './src/store/configureStore'
import AppReducer from './src/reducers'
import AppWithNavigationState from './src/routes'

export default class App extends React.Component {
  render() {
    return (
      <Provider store={store()}>
        <AppWithNavigationState />
      </Provider>
    );
  }
}

